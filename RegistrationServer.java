
import java.net.Socket;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ashay
 */
public class RegistrationServer {
        String username,passwd,password,cnfpasswd;
//      Socket socket;
     Connection conn;
    String name="",sock;
//    DashBoard dashboard = new DashBoard();
    ResultSet rs = null;
    PreparedStatement ps = null;
    PrintWriter output;
    ArrayList<String> nameList;
    RegistrationServer(){
        try{
            ServerSocket server = new ServerSocket(9999);
            while(true){

                Socket client = server.accept();
                new Thread(new Runnable(){
                    public void run(){
                        BufferedReader input;

                        try {
                            output = new PrintWriter(client.getOutputStream(),true);

                            input = new BufferedReader(new InputStreamReader(client.getInputStream()));
                            username = input.readLine();
                            passwd = input.readLine();
                            cnfpasswd = input.readLine();
                            System.out.println("username at server:" + username);
                            validate();
                        } catch (IOException ex) {
                            Logger.getLogger(AuthenticationServer.class.getName()).log(Level.SEVERE, null, ex);
                        }
  
                    }}).start();

            }
        }catch(IOException ie){
                System.out.println("Issue in server : " +ie.getMessage());
        }
    } 
    public void validate(){
            if(passwd.equals(cnfpasswd)){
                                conn = DBConnect.connectDB();        
                        String sql= "INSERT into clients (name,password) VALUES (?,?)";
                            try{
                                ps = conn.prepareStatement(sql);
                                ps.setString(1,username);
                                ps.setString(2,passwd);
                              ps.executeUpdate();
                                
JOptionPane.showMessageDialog(null,"inserted" );
                            }catch(SQLException e){
                                JOptionPane.showMessageDialog(null,"error" + e.getMessage());
                            }catch (Exception ex) {
                                    Logger.getLogger(AuthenticationServer.class.getName()).log(Level.SEVERE, null, ex);
                            }

            }
    }
    public static void main(String[] args){
        new RegistrationServer();
    }    
}