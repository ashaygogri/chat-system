
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ashay
 */
public class MessagingServer {
    PrintWriter output;
    MessagingServer(){
        try{
            ServerSocket server = new ServerSocket(7777);
            HashMap<String,Socket> hashMap = new HashMap<>();
			while(true){

                            Socket client = server.accept();
                            new Thread(new Runnable(){
                                public void run(){
                                     BufferedReader input=null;
                                     String userName ="",recName="",msg="";
//                                     JOptionPane.showMessageDialog(null,"from ms"); 
                                     try {
                                        output = new PrintWriter(client.getOutputStream(),true);

                                        input = new BufferedReader(new InputStreamReader(client.getInputStream()));
//                                        JOptionPane.showMessageDialog(null,client);
                                        
                                        userName = input.readLine();
                                        System.out.println("userName message server: "+userName);
//                                        output.print(userName);
                                        
                                        hashMap.put(userName,client);
                                        System.out.println(hashMap);
                                    } catch (IOException ex) {
                                        Logger.getLogger(MessagingServer.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                            				
                                    new MessageToClient(client,hashMap);

                                }
                            }).start();
                            
                      }
		}catch(IOException ie){
			System.out.println("Issue in server : " +ie.getMessage());
        }
    }

    public static void main(String[] args){
        new MessagingServer();
    }
    
}
