import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Echoer extends Thread {
	private Socket clientSocket;
	PrintWriter output;
        String recName,msg;
        BufferedReader input;
        DashBoard dashboard;
	public Echoer(Socket clientSocket,DashBoard dashboard){
		this.clientSocket = clientSocket;
                this.dashboard = dashboard;
                System.out.println("echoer started");
            try {
                input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));     
            } catch (IOException ex) {
                Logger.getLogger(Echoer.class.getName()).log(Level.SEVERE, null, ex);
            }
                                JOptionPane.showMessageDialog(null,"from echoe:: ");    

		start();
	}
	public void run(){
		try{
			
                    while(true){
                        msg = input.readLine();
                        System.out.println("inside echoe msg: " +msg);
                        dashboard.addMsg(msg,1);
                    }
		}catch(IOException ie){
			System.out.println("issue :" +ie.getMessage());
		}
	}
}
