import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ashay
 */
public class MainFrame extends JFrame {
    crossref crossref;
    SignIn SignIn;
    SignUp SignUp;
    boolean flag=true;
    MainFrame mf;
    MainFrame(){
        super("Skike Chatting App");
//        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
//        this.setLocation(dimension.width/2 - getWidth()/2, dimension.height/2 - getHeight()/2);  
//        this.setLocationRelativeTo(null);
        
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setVisible(true);
    	setSize(775,480);
        setLayout(null);
//    	setResizable(false);
        
        crossref = new crossref();
        crossref.setBounds(0,0,380,450);
        add(crossref);
        
        SignIn = new SignIn(this);
        SignIn.setBounds(380,0,380,450);
        add(SignIn);
        SignIn.updateUI();
        
        SignUp = new SignUp(this);
        SignUp.setBounds(-380,0,380,450);
        add(SignUp);
        SignUp.updateUI();
        
    }
    public static void main(String[]args){
        new MainFrame();
        
    }
    public void animate(){
              new Thread(new Runnable(){
                public void run(){ 
                    while (crossref.getX()<=380  && SignIn.getX()<=760  &&  SignUp.getX()<=0){
                        crossref.setBounds(crossref.getX()+2, crossref.getY(), crossref.getWidth(), crossref.getHeight());
                        SignIn.setBounds(SignIn.getX()+2, SignIn.getY(), SignIn.getWidth(), SignIn.getHeight());
                      
                        SignUp.setBounds(SignUp.getX()+2, SignUp.getY(), SignUp.getWidth(), SignUp.getHeight());
                        repaint();
                        try{
                            Thread.sleep(5);
                        }catch(InterruptedException ie){
                            System.out.println("Exception: "+ie.getStackTrace());
                        }
                    }  
                }
        }).start();
    }
    public void animation1(){
              new Thread(new Runnable(){
                public void run(){
                    while (crossref.getX()>=0  && SignIn.getX()>=380  &&  SignUp.getX()>=-380){
                        crossref.setBounds(crossref.getX()-2, crossref.getY(), crossref.getWidth(), crossref.getHeight());
                        SignIn.setBounds(SignIn.getX()-2, SignIn.getY(), SignIn.getWidth(), SignIn.getHeight());
                        SignUp.setBounds(SignUp.getX()-2, SignUp.getY(), SignUp.getWidth(), SignUp.getHeight());
                        repaint();
                        try{
                            Thread.sleep(5);
                        }catch(InterruptedException ie){
                            System.out.println("Exception: "+ie.getStackTrace());
                        }
                    }
                }
        }).start();
    }
}
